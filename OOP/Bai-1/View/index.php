<?php
$router = (isset($_GET['controller'])) ? $_GET['controller'] : 'Employee';
include_once("Controller/{$router}.php");
?>
<!DOCTYPE html>
<html>

<head>
    <title>Danh sách nhân viên</title>
</head>

<body>
    <br><br><br>
    <form action="#" method='post'>
        <button type="submit" name="statistic">Thống kê</button><br><br>
    </form>
    <?php
    if (isset($_POST['statistic'])) { ?>
        <a href="?controller=create">Thêm nhân viên</a> <br><br>
        *Note: Login before to see login time:
        <a href="?controller=login">Login</a><br><br>
        <a href="?controller=LogTime">Logged time</a><br><br>

        <form action="#" method='post'>
            <input type="text" name="s" placeholder="Enter name here">
            <button type="submit" name="search">Search</button><br><br>
        </form>

        <h1>Danh sách nhân viên</h1>
        <table width="100%" border="1" cellspacing="0" cellpadding="10">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Birthday(age)</td>
                <td>Position</td>
                <td>Salary</td>
                <td>Nation</td>
                <form action="#" method="POST">
                    <td style="font-weight:bold" colspan="2"><button style="width: 100%;" type="submit" name="sort">Sort</button></td>
                </form>

            </tr>

        <?php
        $getEmployee = $this->pageBreak();
    } ?>
        </table>
        <?php
        if (isset($_POST['sort'])) {
            $query = $this->Sort();
        ?>
            <table width="100%" border="1" cellspacing="0" cellpadding="10">
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Birthday</td>
                    <td>Position</td>
                    <td>Salary</td>
                    <td>Nation</td>
                </tr>
                <?php
                while ($row = $query->fetch_assoc()) { ?>

                    <tr>
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $this->computeAge($row['birthday']); ?></td>
                        <td><?php echo $row['position']; ?></td>
                        <td><?php echo $row['salary']; ?></td>
                        <td><?php echo $row['nation']; ?></td>
                        <td><a href="?controller=update&id=<?php echo $row['id']; ?>">Sửa</a></td>
                        <td><a href="?controller=delete&id=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure?');">Xóa</a></td>
                    </tr>
                <?php
                } ?>
            </table>
        <?php
        }

        //Search
        if (isset($_POST['search'])) {
            $name = $_POST['s'];
            $query = $this->search($name);
        ?>
            <table width="100%" border="1" cellspacing="0" cellpadding="10">
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Birthday</td>
                    <td>Position</td>
                    <td>Salary</td>
                    <td>Nation</td>
                </tr>
                <?php
                while ($row = $query->fetch_assoc()) { ?>
                    <tr>
                        <td><?php echo $row['id']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td><?php echo $this->computeAge($row['birthday']); ?></td>
                        <td><?php echo $row['position']; ?></td>
                        <td><?php echo $row['salary']; ?></td>
                        <td><?php echo $row['nation']; ?></td>
                    </tr>
            <?php
                }
            }
            ?>
            </table>
</body>

</html>