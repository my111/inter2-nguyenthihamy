<?php
$id = $_GET['id'];
?>
<!DOCTYPE html>
<html>

<head>
  <title>Edit </title>
</head>

<body>
  <form action="#" method="POST">
    <?php
    $id = $_GET['id'];
    $sql = "SELECT * FROM employee WHERE id = $id";
    $result = $this->model->conn->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      while ($row = $result->fetch_assoc()) { ?>
        <div>
          Name <input type="text" name="name" value="<?php echo $row['name'] ?>">
        </div>
        <div>
          Birthday <input type="date" name="birthday" value="<?php echo $row['birthday'] ?>">
        </div>
        <div>
          Position <select name="sign" size="1">
            <option value=""></option>
            <option value="admin">Admin</option>
            <option value="employer">Employer</option>
            <option value="employee">Employee</option>
          </select>
        </div>
        <div>
          Salary <input type="text" name="salary" value="<?php echo $row['salary'] ?>">
        </div>
        <div>
          Nation <input type="text" name="nation" value="<?php echo $row['nation'] ?>">
        </div>
    <?php
      }
    } ?>
    <button type="submit" name="save">Save</button>
  </form>

  <?php
  if (isset($_POST['save'])) {
    $name = $_POST['name'];
    $birthday = $_POST['birthday'];
    if (isset($_POST['sign'])) {
      switch ($_POST['sign']) {
        case 'admin':
          $position = "admin";
          break;
        case 'employer':
          $position = "employer";
          break;
        case 'employee':
          $position = "employee";
          break;
        case '':
          $sql = "SELECT * FROM employee WHERE id = $id";
          $result = $this->model->conn->query($sql);
          if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
              $position = $row['position'];
            }
          }
      }
    }

    $salary = $_POST['salary'];
    $nation = $_POST['nation'];
    $edit = $this->editEmployee($id, $name, $birthday, $position, $salary, $nation);
    if ($edit) {
      echo "<script>alert('Information updated!')</script>";
      header('Refresh:0.5; url=index.php');
    }
  }
  ?>
</body>

</html>