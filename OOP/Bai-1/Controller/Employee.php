<?php
session_start();
class Employee
{
    protected $model;
    protected $template = "View/index.php";

    public function __construct()
    {
        $this->model = new Database();
        include_once($this->template);
    }

    public function addEmployee($name, $birthday, $position, $salary, $nation)
    {
        $sql = "INSERT INTO employee(name, birthday, position, salary, nation) VALUES ('$name', '$birthday', '$position', '$salary', '$nation')";
        $query = $this->model->conn->query($sql);
        return $sql;
    }

    public function editEmployee($id, $name, $birthday, $position, $salary, $nation)
    {
        $sql = "UPDATE employee SET name='$name', birthday='$birthday', position='$position', salary='$salary', nation='$nation' WHERE id ='$id'";
        $query = $this->model->conn->query($sql);
        return $query;
    }

    public function deleteEmployee($id)
    {
        $sql = "DELETE FROM employee WHERE id= $id";
        $query = $this->model->conn->query($sql);
        return $query;
    }

    function computeAge($birthday)
    {
        return intval(date('Y', time() - strtotime($birthday))) - 1970;  //Subtract from 1970 because strtotime calculates time from 1970-01-01
    }

    function pageBreak()
    {
        //find total record
        $query = $this->model->conn->query("SELECT count(id) as total FROM employee");
        $row = $query->fetch_assoc();
        $totalRecord = $row['total'];

        //set limit and find current page
        $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = 10;

        //find total page and start
        $totalPage = ceil($totalRecord / $limit);   //round result to int value

        //limit current page from 1 to $totalPage
        if ($currentPage > $totalPage) {
            $currentPage = $totalPage;
        } else if ($currentPage < 1) {
            $currentPage = 1;
        }

        //start
        $start = ($currentPage - 1) * $limit;

        //take list
        $result = $this->model->conn->query("SELECT * FROM employee LIMIT $start,$limit");

        //display
        while ($row = $result->fetch_assoc()) { ?>
            <tr>
                <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $this->computeAge($row['birthday']); ?></td>
                <td><?php echo $row['position']; ?></td>
                <td><?php echo $row['salary']; ?></td>
                <td><?php echo $row['nation']; ?></td>
                <td><a href="?controller=update&id=<?php echo $row['id']; ?>">Sửa</a></td>
                <td><a href="?controller=delete&id=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure?');">Xóa</a></td>
            </tr>
        <?php }

        //condition for pre button
        if ($currentPage > 1 && $totalPage > 1)
            echo '<a href="index.php?page=' . ($currentPage - 1) . '">Prev</a> | ';
        //repeat the middle
        for ($i = 1; $i < -$totalPage; $i++) {
            if ($i == $currentPage) {
                echo '<span>' . $i . '</span> | ';
            } else
                echo '<a href="index.php?page=' . $i . '">' . $i . '</a> | ';
        }

        //condition for next button
        if ($currentPage < $totalPage && $totalPage > 1) {
            echo '<a href="index.php?page=' . ($currentPage + 1) . '">Next</a> | ';
        } ?>
<?php
    }

    function search($name)
    {
        $query = $this->model->conn->query("SELECT * FROM employee WHERE name = '$name'");
        return $query;
    }

    function Sort()
    {
        $query = $this->model->conn->query("SELECT * FROM employee ORDER BY name");
        return $query;
    }
}

new Employee();
