<!DOCTYPE html>
<html>

<head>
  <title>Add Employee</title>
</head>

<body>
  <form action="#" method="POST">
    <div>
      Name <input type="text" name="name" required>
    </div>
    <div>
      Birthday <input type="date" name="birthday" required>
    </div>
    <div>
      Position <select name="sign" size="1">
        <option value=""></option>
        <option value="admin">Admin</option>
        <option value="employer">Employer</option>
        <option value="employee">Employee</option>
      </select>
    </div>
    <div>
      Salary <input type="text" name="salary">
    </div>
    <div>
      Nation <input type="text" name="nation">
    </div>

    <button type="submit" name="add">Add</button>
  </form>

  <?php
  if (isset($_POST['add'])) {
    $name = $_POST['name'];
    $birthday = $_POST['birthday'];
    if (!empty($_POST['sign'])) {
      switch ($_POST['sign']) {
        case 'admin':
          $position = "admin";
          break;
        case 'employer':
          $position = "employer";
          break;
        case 'employee':
          $position = "employee";
          break;
      }
    }
    $salary = $_POST['salary'];
    $nation = $_POST['nation'];
    $employee = $this->addEmployee($name, $birthday, $position, $salary, $nation);
    if ($employee)
      header('location:index.php');
  }
  ?>
</body>

</html>