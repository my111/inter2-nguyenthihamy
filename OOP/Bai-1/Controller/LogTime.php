<!DOCTYPE html>
<html>

<body>
    <table border="1" style="border-collapse:collapse">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Logged time</th>
            </tr>
        </thead>

        <tbody>
            <?php
            $result = $this->model->conn->query("SELECT * FROM employee");
            while ($row = $result->fetch_assoc()) { ?>
                <tr>
                    <td><?php echo $row['id'] ?></td>
                    <td><?php echo date("Y-m-d") ?></td>
                    <td><?php echo $row['loginTime'] ?></td>
                </tr>
            <?php
            }
            if (isset($_POST['statistic']))
                header('location:index.php');
            ?>
        </tbody>
    </table>
</body>

</html>