<?php
$id = $_GET['id'];
$getEmployee = new Employee();
$employee = $getEmployee->deleteEmployee($id);
if ($employee) {
    echo "<script>alert('Account deleted!')</script>";
    header('Refresh: 0.5; url=index.php');
}
