<?php
class Database
{
  protected $hn = 'localhost';
  protected $db = 'public';
  protected $un = 'root';
  protected $pw = '';
  public $conn = null;

  public function __construct()
  {
    $this->conn = mysqli_connect($this->hn, $this->un, $this->pw, $this->db);
    if ($this->conn->connect_error) {
      die("Fail" . $this->conn->connect_error);
    }
  }
}
