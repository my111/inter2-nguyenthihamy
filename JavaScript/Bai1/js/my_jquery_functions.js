var i;
var tabcontent = document.getElementsByClassName("tabcontent");
var tablinks = document.getElementsByClassName("tablinks");
var currentTab=1;
document.addEventListener("DOMContentLoaded", function(event) { 
  openTab(1);
});


function openTab(currentTab) {
  //ẩn nội dung các tab
  hideTab();

  //xóa các active tab
  deletecurrentTab();

  //hiện tab được click vào
  displayTab(currentTab);
  tablinks[currentTab-1].className += " active";

}

function changeTab(index) {
  currentTab = currentTab + index;
  if (currentTab > tabcontent.length) {
    currentTab = 1;
  }
  if (currentTab < 1) {
    currentTab = tabcontent.length;
  }
  hideTab();
  displayTab(currentTab);
  deletecurrentTab();
  tablinks[currentTab-1].className += " active";

}

function hideTab() {
  
  // for (i = 0; i < tabcontent.length; i++) {
  //   tabcontent[i].style.display = "none";
  // }

  if (document.querySelector(".tabcontent.active")) {
    document.querySelector(".tabcontent.active").style.display= "none";
    document.querySelector(".tabcontent.active").classList.remove("active");
  }
}


function deletecurrentTab() {
//  for (i = 0; i < tablinks.length; i++) {
//    tablinks[i].className = tablinks[i].className.replace(" active", "");
//   }
  if (document.querySelector(".tablinks.active")) {
    document.querySelector(".tablinks.active").classList.remove("active");
  }
  
}

function displayTab(currentTab) {
  // tabcontent[currentTab-1].style.display = "block";
  tabcontent[currentTab-1].className += " active";
  document.querySelector(".tabcontent.active").style.display= "block";

}
