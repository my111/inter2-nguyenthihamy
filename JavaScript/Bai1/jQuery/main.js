var index=0;
$(function () {

    $(".tabs button").click(function () {
        var tabId = this.id;
        var contentId = this.id + "_content";
        $("button").removeClass("selected");
        $("button#" + tabId).addClass("selected");

        $("article").removeClass("selected");
        $("article#" + contentId).addClass("selected");
        index = $('.selected').index();
    });
});
$(function () {

    $(".tabs a").click(function () {
        if (this.id == 'prev') {
            index = (index - 1) % ($('button').length);
        } else if (this.id == 'next') {
            index = (index + 1) % ($('button').length);
        }
        $("button").removeClass("selected");
        $("button").eq(index).addClass("selected");

        $("article").removeClass("selected");
        $("article").eq(index).addClass("selected");
    });
});
