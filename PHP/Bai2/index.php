<!DOCTYPE html>
<html>

<head>
    <title>Bai2</title>
    <script src="http://code.jquery.com/jquery-3.2.0.min.js"></script>
</head>

<body>
    <table width="270px" cellspacing="0px" cellpadding="0px" border="1px">
        <?php
        for ($row = 1; $row <= 8; $row++) {
            echo "<tr>";
            for ($col = 1; $col <= 8; $col++) {
                $total = $row + $col;
                $position = ($row - 1) * 8 + $col;
                if ($total % 2 == 0) {
                    echo "<td id=$position height=30px width=30px bgcolor=#FFFFFF></td>";
                } else {
                    echo "<td id=$position height=30px width=30px bgcolor=#000000></td>";
                }
            }
            echo "</tr>";
        }
        $position = [];
        $rows = [];
        $cols = [];
        while (count($position) != 8) {   // tim 8 vi tri ngau nhien khac nhau
            $randomNumber = mt_rand(1, 64);
            
            //vi tri hang va cot
            if ($randomNumber % 8 == 0) {
                $row = (int)($randomNumber / 8);
                $col = 8;
            } else {
                $row = (int)($randomNumber / 8) + 1;
                $col = $randomNumber % 8;
            }
            if (!in_array($randomNumber, $position))
                array_push($position, $randomNumber);
            array_push($rows, $row);
            array_push($cols, $col);
        ?>
            <script>
                $("#<?php echo $randomNumber; ?>").css("background", "green");
            </script>
            <?php
        }

        //khong cung hang, cot , duong cheo voi cac quan khac
        for ($i = 0; $i < 8; $i++) {
            $count = 0;
            for ($j = 0; $j < 8; $j++) {
                if ($rows[$i] != $rows[$j]) {
                    if ($cols[$i] != $cols[$j]) {
                        if (abs($rows[$i] - $rows[$j]) != abs($cols[$i] - $cols[$j])) {
                            $count++;
                        }
                    }
                }
            }
            if ($count == 7) {
            ?>
                <script>
                    $("#<?php echo $position[$i]; ?>").css("background", "red");
                </script>
        <?php
            }
        }
        ?>
    </table>
</body>

</html>