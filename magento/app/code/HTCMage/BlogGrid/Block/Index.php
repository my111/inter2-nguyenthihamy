<?php
 
namespace HTCMage\BlogGrid\Block;
 
use Magento\Framework\View\Element\Template;
 
class Index extends \Magento\Framework\View\Element\Template
{
    protected $_storeManager;
    public function __construct(
        Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = [])
{
       $this->_storeManager = $storeManager;
       parent::__construct($context, $data);
}

public function getMediaURL()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }
}
